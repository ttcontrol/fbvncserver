/**************************************************************************
 * Copyright (c) 2015 TTControl. All rights reserved. Confidential proprietary
 * Schönbrunnerstraße 7, A-1040 Wien, Austria. office@ttcontrol.com
 **************************************************************************/

#include <stdio.h>
#include "vnc.h"

#define DBUS_DESTINATION_VNC "com.ttcontrol.vnc"
#define DBUS_PATH_VNC "/com/ttcontrol/vnc"
#define DBUS_IFACE_VNC "com.ttcontrol.vnc"

static vnc_connect_callback on_connect;
static vnc_disconnect_callback on_disconnect;

static DBusHandlerResult vnc_message_func(DBusConnection *connection,
                                      DBusMessage *message, void *data)
{
    DBusConnection *conn = data;

    if (dbus_message_is_signal(message, DBUS_IFACE_VNC, "ClientNew") && on_connect != NULL)
    {
        on_connect();
    }
    else if (dbus_message_is_signal(message, DBUS_IFACE_VNC, "ClientGone") && on_disconnect != NULL)
    {
        on_disconnect();
    }

    return DBUS_HANDLER_RESULT_HANDLED;
}

static void vnc_unregister_func(DBusConnection *connection, void *data)
{
}

static DBusObjectPathVTable vnc_dbus_vtable = {
    .unregister_function = vnc_unregister_func,
    .message_function = vnc_message_func,
};

unsigned char vnc_register(DBusConnection *connection)
{
    DBusError err;

    dbus_error_init(&err);
    dbus_bus_add_match(connection, "type='signal',interface='" DBUS_IFACE_VNC
        "',member='ClientNew'", &err);
    dbus_bus_add_match(connection, "type='signal',interface='" DBUS_IFACE_VNC
        "',member='ClientGone'", &err);
    if (dbus_error_is_set(&err))
    {
        printf("DBus Match error: %s\n", err.message);
        return 0;
    }

    if (!dbus_connection_register_object_path(connection, DBUS_PATH_VNC,
        &vnc_dbus_vtable, connection))
    {
        printf("register error\n");
        return 0;
    }

    return 1;
}

void vnc_unregister(DBusConnection *connection)
{
    dbus_bus_remove_match(connection, "type='signal',interface='" DBUS_IFACE_VNC
        "',member='ClientNew'", NULL);
    dbus_bus_remove_match(connection, "type='signal',interface='" DBUS_IFACE_VNC
        "',member='ClientGone'", NULL);
    dbus_connection_unregister_object_path(connection, DBUS_PATH_VNC);
}

unsigned char vnc_start(DBusConnection* connection, char * relay_ip,
    int relay_port, int * id, vnc_connect_callback connect_callback,
    vnc_disconnect_callback disconnect_callback)
{
    DBusMessage* msg;
    DBusError err;
    DBusPendingCall* pending;
    unsigned char result;
    char * error_msg;
    
    on_connect = connect_callback;
    on_disconnect = disconnect_callback;

    msg = dbus_message_new_method_call(DBUS_DESTINATION_VNC, DBUS_PATH_VNC,
        DBUS_IFACE_VNC, "Start");
    if (msg == NULL)
    {
        printf("vnc_start: Could not call method\n");
        return 0;
    }

    if (!dbus_message_append_args(msg, DBUS_TYPE_STRING, &relay_ip,
        DBUS_TYPE_UINT32, &relay_port, DBUS_TYPE_INT32, id, DBUS_TYPE_INVALID))
    {
        printf("vnc_start: Could not build message\n");
        return 0;
    }

    if (!dbus_connection_send_with_reply(connection, msg, &pending, -1))
    {
        printf("vnc_start: OOM while sending\n");
        dbus_message_unref(msg);
        return 0;
    }
    if (pending == NULL)
    {
        printf("vnc_start: Pending call NULL\n");
        dbus_message_unref(msg);
        return 0;
    }
    dbus_message_unref(msg);

    dbus_pending_call_block(pending);

    *id = -1;
    msg = dbus_pending_call_steal_reply(pending);
    if (msg == NULL)
    {
        printf("vnc_start: reply NULL\n");
        return 0;
    }
    dbus_pending_call_unref(pending);

    dbus_error_init(&err);
    
    if (dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_ERROR)
    {
        dbus_message_get_args(msg, &err, DBUS_TYPE_STRING, &error_msg,
            DBUS_TYPE_INVALID);
        printf("vnc_start: %s\n", error_msg);
    }
    else
    {
        if (!dbus_message_get_args(msg, &err, DBUS_TYPE_BOOLEAN, &result,
            DBUS_TYPE_INT32, id, DBUS_TYPE_INVALID))
        {
            printf("vnc_start: Could not read reply. %s\n", err.message);
            result = 0;
        }
    }

    dbus_message_unref(msg);
    return result;
}

unsigned char vnc_stop(DBusConnection* connection)
{
    DBusMessage* msg;
    DBusError err;
    DBusPendingCall* pending;
    unsigned char result;
    char * error_msg;

    msg = dbus_message_new_method_call(DBUS_DESTINATION_VNC, DBUS_PATH_VNC,
        DBUS_IFACE_VNC, "Stop");
    if (msg == NULL)
    {
        printf("vnc_stop: message NULL\n");
        return 0;
    }

    if (!dbus_connection_send_with_reply(connection, msg, &pending, -1))
    {
        printf("vnc_stop: OOM while sending\n");
        return 0;
    }
    if (pending == NULL)
    {
        printf("vnc_stop: Pending call NULL\n");
        return 0;
    }

    dbus_pending_call_block(pending);
    dbus_message_unref(msg);

    msg = dbus_pending_call_steal_reply(pending);
    if (msg == NULL)
    {
        printf("vnc_stop: reply NULL\n");
        return 0;
    }
    dbus_pending_call_unref(pending);

    dbus_error_init(&err);
    if (dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_ERROR)
    {
        dbus_message_get_args(msg, &err, DBUS_TYPE_STRING, &error_msg,
            DBUS_TYPE_INVALID);
        printf("vnc_start: %s\n", error_msg);
    }
    else
    {
        if (!dbus_message_get_args(msg, &err, DBUS_TYPE_BOOLEAN, &result,
            DBUS_TYPE_INVALID))
        {
            printf("vnc_stop: Could not read reply. %s\n", err.message);
            result = 0;
        }
    }
    dbus_message_unref(msg);
    return result;
}
