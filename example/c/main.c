/**************************************************************************
 * Copyright (c) 2015 TTControl. All rights reserved. Confidential proprietary
 * Schönbrunnerstraße 7, A-1040 Wien, Austria. office@ttcontrol.com
 **************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <dbus/dbus.h>
#include <libhal.h>
#include "vnc.h"

// Define this to only open a local listening port
//#define LOCAL_VNC_ONLY

#ifdef LOCAL_VNC_ONLY
    #define RELAY_IP    ""
    #define RELAY_PORT  0
#else
    // Set to IP or domain name of relay server
    #define RELAY_IP    "10.100.31.13"
    // Set port of relay server
    #define RELAY_PORT  5500
#endif

static void on_connect()
{
    printf("Client connected!\n");
}

static void on_disconnect()
{
    printf("Client disconnected!\n");
}

static int dbus_running, app_running;

static void sigint_handler(int arg)
{
    app_running = 0;
}

int main(int argc, char **argv)
{
    struct dbus_ctx* ctx;
    struct event_base* base;
    int id = 0;
    DBusConnection *conn;

    signal(SIGINT, sigint_handler);

    // Initialize necessary HAL modules
    hal_init();
    hal_dbus_init();
    hal_dbus_get_system_bus(&conn);

    // Register the VNC server; the server daemon will now be started
    // automatically, but no connection is possible.
    printf("Register VNC server\n");
    vnc_register(conn);

#ifdef LOCAL_VNC_ONLY
    id = -1;
#endif

    // Start VNC server. If a relay server is specified a connection is
    // is established. A local listening port is opened in any case.
    // The ID returned must be presented to the user. It is necessary for
    // the VNC client to connect. If the returned value is 0 no connection
    // to the relay was possible.
    if (vnc_start(conn, RELAY_IP, RELAY_PORT, &id, on_connect, on_disconnect))
    {
        printf("Server started, id = %d\n", id);
    }
    else
    {
        printf("Could not start server\n");
    }

    // Wait until application is terminated using Ctrl+C
    app_running = 1;
    while (app_running)
    {
        sleep(1);
    }

    // Stop the VNC server. All connections are terminated. The VNC daemon will
    // continue running.
    if (!vnc_stop(conn))
    {
        printf("Could not stop server!\n");
    }
    else
    {
        printf("Server stopped.\n");
    }
    
    // Unregister VNC and close HAL.
    vnc_unregister(conn);
    hal_dbus_deinit();
    hal_deinit();

    return 0;
}
