/**************************************************************************
 * Copyright (c) 2015 TTControl. All rights reserved. Confidential proprietary
 * Schönbrunnerstraße 7, A-1040 Wien, Austria. office@ttcontrol.com
 **************************************************************************/

#pragma once

#include <dbus/dbus.h>

typedef void (*vnc_connect_callback)();
typedef void (*vnc_disconnect_callback)();

/** \defgroup wlan WLAN support
 *
 *  \{
 */

/** \brief Register VNC server
 *
 *  Register the VNC server with the DBus module of HAL. Before this function
 *  is called, hal_dbus_init() must have been called successfully.
 *  Must be called before the server can be started or stopped. The VNC daemon
 *  will be launched upon this call.
 *
 *  \param[in]  connection        The DBus system bus. This value can be
 *                                obtained by calling hal_dbus_get_system_bus().
 *
 *  \return Returns TRUE on success, FALSE otherwise.
 */
unsigned char vnc_register(DBusConnection* connection);

/** \brief Unregister VNC server
 *
 *  Unregister the VNC server with the DBus module of HAL. The VNC daemon will
 *  continue running.
 *
 *  \param[in]  connection        The DBus system bus. This value can be
 *                                obtained by calling hal_dbus_get_system_bus().
 *
 *  \return Returns TRUE on success, FALSE otherwise.
 */
void vnc_unregister(DBusConnection *connection);

/** \brief Start VNC server
 *
 *  Start the VNC server. Will open a local listening socket on port 5901. If
 *  a relay server is specified a connection to this server will be made.
 *  If #id is zero no connection to the relay server was possible. The local
 *  VNC connection is started nevertheless.
 *  If #relay_ip is an empty string no relay server will be used and only a
 '  local listening port is opened.
 *  The #id is valid until #vnc_stop is called; this means several connections
 *  from the client are possible with the same ID.
 *
 *  To connect the VNC server, please use Hextile encoding for best performance.
 *
 *  \param[in]  connection           The DBus system bus. This value can be
 *                                   obtained by calling hal_dbus_get_system_bus().
 *  \param[in]  relay_ip             IP oder domain name of relay server.
 *  \param[in]  relay_port           Port of relay service. Usually 5500.
 *  \param[out] id                   The connection ID. Must be presented to the
 *                                   user. Zero on error.
 *  \param[in]  connect_callback     Function to call if a client connects. Can
 *                                   be NULL.
 *  \param[in]  disconnect_callback  Function to call if a client disconnects.
 *                                   Can be NULL.
 *
 *  \return Returns TRUE on success, FALSE otherwise.
 */
unsigned char vnc_start(DBusConnection* connection, char * relay_ip,
    int relay_port, int * id, vnc_connect_callback connect_callback,
    vnc_disconnect_callback disconnect_callback);

/** \brief Stop VNC server
 *
 *  Closes all connections.
 *
 *  \param[in]  connection        The DBus system bus. This value can be
 *                                obtained by calling hal_dbus_get_system_bus().
 *
 *  \return Returns TRUE on success, FALSE otherwise.
 */
unsigned char vnc_stop(DBusConnection* connection);

/** \} */