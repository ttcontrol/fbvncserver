/*
 * fbvncserver.c
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * Started with original fbvncserver for the iPAQ and Zaurus.
 *  http://fbvncserver.sourceforge.net/
 *
 * Modified by Jim Huang <jserv.tw@gmail.com>
 *  - Simplified and sizing down
 *  - Performance tweaks
 *
 * Modified by Steve Guo (letsgoustc)
 *  - Added keyboard/pointer input
 *
 * Modified by Danke Xie (danke.xie@gmail.com)
 *  - Added input device search to support different devices
 *  - Added kernel vnckbd driver to allow full-keyboard input on 12-key hw
 *  - Supports Android framebuffer double buffering
 *  - Performance boost and fix GCC warnings in libvncserver-0.9.7
 *
 * NOTE: depends libvncserver.
 */

/* define the following to enable debug messages */
//#define DEBUG
//#define DEBUG_VERBOSE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <linux/fb.h>
#include <linux/input.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>

#include <pthread.h>
#include <dbus/dbus.h>

/* libvncserver */
#include "rfb/rfb.h"
#include "rfb/keysym.h"
#include "rfb/rfbregion.h"

#define APPVER "1.1"
#define APPNAME "fbvncserver"
#define VNC_DESKTOP_NAME "Vision2"

/* framebuffer */
#ifdef ANDROID
# define FB_DEVICE "/dev/graphics/fb0"
#else
# define FB_DEVICE "/dev/fb0"
#endif

#define NUM_KEYDEVS 2
#define INPUTDEV_KEYPAD 0
#define INPUTDEV_ENCODER 1

/* default input device paths */
static char KEY_DEVICE[NUM_KEYDEVS][PATH_MAX] = { "/dev/input/keypad", "/dev/input/encoder" };

static struct fb_var_screeninfo scrinfo;
static int fbfd = -1;
static int keyfd[NUM_KEYDEVS] = { -1, -1 };
static int touchfd = -1;
static unsigned short int *fbmmap = MAP_FAILED;
static unsigned short int *vncbuf;
static unsigned short int *fbbuf;
static int pixels_per_int;

__sighandler_t old_sigint_handler = NULL;

#define VNC_PORT 5901
static rfbScreenInfoPtr vncscr;

static char * relay_server_ip = NULL;
static unsigned int relay_port = 0;
static int relay_id = -1;
static int use_dbus = 0;

static pthread_t vnc_thread;
static unsigned char vnc_running = 0;

static int xmin, xmax;
static int ymin, ymax;

/* part of the frame differerencing algorithm. */
static struct varblock_t {
    int min_x;
    int min_y;
    int max_x;
    int max_y;
    int r_offset;
    int g_offset;
    int b_offset;
    int pixels_per_int;
} varblock;

/* event handler callback */
static void keyevent(rfbBool down, rfbKeySym key, rfbClientPtr cl);
static void ptrevent(int buttonMask, int x, int y, rfbClientPtr cl);
static enum rfbNewClientAction client_new(rfbClientPtr cl);

static void* vnc_loop();

#define DBUS_DESTINATION "com.ttcontrol.vnc"
#define DBUS_PATH "/com/ttcontrol/vnc"
#define DBUS_IFACE "com.ttcontrol.vnc"

static DBusConnection* dbusConn;
static DBusMessage* startServerReply;

#ifdef DEBUG
# define pr_debug(fmt, ...) \
   fprintf(stderr, fmt, ## __VA_ARGS__)
#else
# define pr_debug(fmt, ...) do { } while(0)
#endif

#ifdef DEBUG_VERBOSE
# define pr_vdebug(fmt, ...) \
  pr_debug(fmt, ## __VA_ARGS__)
#else
# define pr_vdebug(fmt, ...) do { } while(0)
#endif

#define pr_info(fmt, ...) \
    fprintf(stdout, fmt, ## __VA_ARGS__)

#define pr_err(fmt, ...) \
 fprintf(stderr, fmt, ## __VA_ARGS__)

static void init_fb(void)
{
    size_t pixels;
    size_t bytespp;

    if ((fbfd = open(FB_DEVICE, O_RDONLY)) == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    if (ioctl(fbfd, FBIOGET_VSCREENINFO, &scrinfo) != 0) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }

    // Mmap all buffers; need virtual size as there might be some padding
    // included (alignment stuff)
    pixels = scrinfo.xres_virtual * scrinfo.yres_virtual;
    bytespp = scrinfo.bits_per_pixel / 8;

    pr_info("xres=%d, yres=%d, "
            "xresv=%d, yresv=%d, "
            "xoffs=%d, yoffs=%d, "
            "bpp=%d\n",
      (int)scrinfo.xres, (int)scrinfo.yres,
      (int)scrinfo.xres_virtual, (int)scrinfo.yres_virtual,
      (int)scrinfo.xoffset, (int)scrinfo.yoffset,
      (int)scrinfo.bits_per_pixel);

    fbmmap = mmap(NULL, pixels * bytespp,
            PROT_READ, MAP_SHARED, fbfd, 0);

    if (fbmmap == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }
}

static void cleanup_fb(void)
{
    if(fbfd != -1)
    {
        close(fbfd);
    }
}

static void init_keys()
{
    int i;

    for (i = 0; i < NUM_KEYDEVS; i++)
    {
        if((keyfd[i] = open(KEY_DEVICE[i], O_RDWR)) == -1)
        {
            pr_info("cannot open input device %s\n", KEY_DEVICE[i]);
        }
    }
}

static void cleanup_keys()
{
    int i;

    for (i = 0; i < NUM_KEYDEVS; i++)
    {
        if (keyfd[i] != -1)
        {
            close(keyfd[i]);
        }
    }
}

static int init_touch()
{
    if ((touchfd = open("/dev/input/touchscreen", O_RDWR)) == -1)
    {
        pr_info("cannot open touch device\n");
    }
}

static void cleanup_touch()
{
    if (touchfd != -1)
    {
        close(touchfd);
    }
}

static void init_dbus()
{
    int ret;
    DBusError dbusErr;

    dbus_threads_init_default();
    dbus_error_init(&dbusErr);
    dbusConn = dbus_bus_get(DBUS_BUS_SYSTEM, &dbusErr);
    if (dbus_error_is_set(&dbusErr))
    {
        pr_err("DBus connection error: %s\n", dbusErr.message);
        dbus_error_free(&dbusErr);
        exit(-2);
    }
    if (dbusConn == NULL)
    {
        pr_err("DBus: connection NULL\n");
        exit(-2);
    }

    ret = dbus_bus_request_name(dbusConn, DBUS_DESTINATION,
        DBUS_NAME_FLAG_REPLACE_EXISTING, &dbusErr);
    if (dbus_error_is_set(&dbusErr))
    {
        pr_err("DBus Name Error: %s\n", dbusErr.message);
        dbus_error_free(&dbusErr);
        exit(-2);
    }
    if (ret != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER)
    {
        pr_err("DBus: not primary name owner\n");
        exit(-2);
    }
}

static void init_fb_server(int argc, char **argv)
{
    pr_info("Initializing server...\n");

    /* Allocate the VNC server buffer to be managed (not manipulated) by
     * libvncserver. */
    vncbuf = calloc(scrinfo.xres * scrinfo.yres, scrinfo.bits_per_pixel / 2);
    assert(vncbuf != NULL);

    /* Allocate the comparison buffer for detecting drawing updates from frame
     * to frame. */
    fbbuf = calloc(scrinfo.xres * scrinfo.yres, scrinfo.bits_per_pixel / 2);
    assert(fbbuf != NULL);

    /* FIXME: This assumes scrinfo.bits_per_pixel is 16. */
    vncscr = rfbGetScreen(&argc, argv, scrinfo.xres, scrinfo.yres,
            5, /* bits per sample */
            2, /* samples per pixel */
            2  /* bytes/sample */ );

    assert(vncscr != NULL);

    vncscr->desktopName = VNC_DESKTOP_NAME;
    vncscr->frameBuffer = (char *)vncbuf;
    vncscr->alwaysShared = TRUE;
    vncscr->httpDir = NULL;
    vncscr->port = VNC_PORT;

    vncscr->kbdAddEvent = keyevent;
    vncscr->ptrAddEvent = ptrevent;
    vncscr->newClientHook = client_new;

    rfbInitServer(vncscr);

    /* Mark as dirty since we haven't sent any updates at all yet. */
    rfbMarkRectAsModified(vncscr, 0, 0, scrinfo.xres, scrinfo.yres);

    /* Bit shifts */
    varblock.r_offset = scrinfo.red.offset + scrinfo.red.length - 5;
    varblock.g_offset = scrinfo.green.offset + scrinfo.green.length - 5;
    varblock.b_offset = scrinfo.blue.offset + scrinfo.blue.length - 5;
    varblock.pixels_per_int = 8 * sizeof(int) / scrinfo.bits_per_pixel;
}

void injectKeyEvent(int device, uint16_t code, uint16_t value)
{
    struct input_event ev;

    assert(device < NUM_KEYDEVS);

    memset(&ev, 0, sizeof(ev));
    gettimeofday(&ev.time,0);
    ev.type = EV_KEY;
    ev.code = code;
    ev.value = value;

    if(write(keyfd[device], &ev, sizeof(ev)) < 0)
    {
        pr_err("write event to %s failed, %s\n", KEY_DEVICE[device], strerror(errno));
    }

    pr_vdebug("injectKey (%s, %d, %d)\n", KEY_DEVICE[device], code, value);
}

static void keyevent(rfbBool down, rfbKeySym key, rfbClientPtr cl)
{
    static rfbBool last_down = FALSE;
    static rfbKeySym last_key = XK_VoidSymbol;
    int scancode;
    int device;

    pr_vdebug("last_key=%d,key=%d  last_down=%d,down=%d\n", last_key, key, last_down, down);

    if ((last_key == XK_VoidSymbol) || ((last_key == key) && (last_down != down)))
    {
        pr_vdebug("Got keysym: %04x (down=%d)\n", (unsigned int)key, (int)down);

        switch (key) {
            case XK_F1: scancode = KEY_F1; device = INPUTDEV_KEYPAD; break;
            case XK_F2: scancode = KEY_F2; device = INPUTDEV_KEYPAD; break;
            case XK_F3: scancode = KEY_F3; device = INPUTDEV_KEYPAD; break;
            case XK_F4: scancode = KEY_F4; device = INPUTDEV_KEYPAD; break;
            case XK_F5: scancode = KEY_F5; device = INPUTDEV_KEYPAD; break;
            case XK_F6: scancode = KEY_F6; device = INPUTDEV_KEYPAD; break;
            case XK_F7: scancode = KEY_F7; device = INPUTDEV_KEYPAD; break;
            case XK_F8: scancode = KEY_F8; device = INPUTDEV_KEYPAD; break;
            case XK_F9: scancode = KEY_F9; device = INPUTDEV_KEYPAD; break;
            case XK_F10: scancode = KEY_F10; device = INPUTDEV_KEYPAD; break;
            case XK_Return: scancode = KEY_ENTER; device = INPUTDEV_ENCODER; break;
            case XK_Up: scancode = KEY_UP; device = INPUTDEV_ENCODER; break;
            case XK_Down: scancode = KEY_DOWN; device = INPUTDEV_ENCODER; break;
            default: scancode = 0; break;
        }

        if (scancode)
        {
            injectKeyEvent(device, scancode, down);
        }

        last_down = down;
        last_key = (down) ? key : XK_VoidSymbol;
    }
}

void injectTouchEvent(int down, int x, int y)
{
    struct input_event ev;

    memset(&ev, 0, sizeof(ev));
    gettimeofday(&ev.time,0);

    ev.type = EV_ABS;

    ev.code = ABS_PRESSURE;
    ev.value = down;
    if(write(touchfd, &ev, sizeof(ev)) < 0)
    {
        pr_err("write event failed, %s\n", strerror(errno));
    }

    ev.code = ABS_HAT0X;
    ev.value = x;
    if(write(touchfd, &ev, sizeof(ev)) < 0)
    {
       pr_err("write event failed, %s\n", strerror(errno));
    }

    ev.code = ABS_HAT0Y;
    ev.value = y;
    if(write(touchfd, &ev, sizeof(ev)) < 0)
    {
       pr_err("write event failed, %s\n", strerror(errno));
    }

    ev.type = EV_SYN;
    ev.code = 0;
    ev.value = 0;
    if(write(touchfd, &ev, sizeof(ev)) < 0)
    {
        pr_err("write event failed, %s\n", strerror(errno));
    }

    pr_vdebug("injectTouchEvent (x=%d, y=%d, down=%d)\n", x , y, down);
}

static void ptrevent(int buttonMask, int x, int y, rfbClientPtr cl)
{
    static int lastMask = 0;

    pr_vdebug("Got ptrevent: %04x (x=%d, y=%d)\n", buttonMask, x, y);

    if (buttonMask & 1)
    {
        injectTouchEvent(1, x, y);
    }
    else if ((lastMask & 1) && !(buttonMask & 1))
    {
        injectTouchEvent(0, x, y);
    }

    lastMask = buttonMask;
}

static void client_gone(rfbClientPtr cl)
{
    DBusMessage* msg;
    dbus_uint32_t serial = 0;

    if (dbusConn == NULL)
    {
        return;
    }

    msg = dbus_message_new_signal(DBUS_PATH, DBUS_IFACE, "ClientGone");
    if (msg == NULL)
    {
        pr_err("ClientGone: Message NULL\n");
    }
    else
    {
        if (!dbus_connection_send(dbusConn, msg, &serial))
        {
            pr_err("ClientGone: OOM\n");
        }
        dbus_message_unref(msg);
    }
}

static enum rfbNewClientAction client_new(rfbClientPtr cl)
{
    DBusMessage* msg;
    dbus_uint32_t serial = 0;

    /* Do not send a message if no protocol version. This only indicates
       that we are connected to the repeater...
       Also do this if program not controlled via DBus. */
    if ((cl == NULL) || (cl->protocolMajorVersion == 0) || (dbusConn == NULL))
        return RFB_CLIENT_ACCEPT;

    cl->clientGoneHook = client_gone;

    msg = dbus_message_new_signal(DBUS_PATH, DBUS_IFACE, "ClientNew");
    if (msg == NULL)
    {
        pr_err("ClientNew: Message NULL\n");
        return RFB_CLIENT_ACCEPT;
    }

    if (!dbus_connection_send(dbusConn, msg, &serial))
    {
        pr_err("ClientNew: OOM\n");
        dbus_message_unref(msg);
        return RFB_CLIENT_ACCEPT;
    }

    return RFB_CLIENT_ACCEPT;
}

static void reply_msg_start(DBusMessage* reply, unsigned char result, int id)
{
    dbus_uint32_t serial = 0;

    if (!dbus_message_append_args(reply, DBUS_TYPE_BOOLEAN, &result,
        DBUS_TYPE_INT32, &id, DBUS_TYPE_INVALID))
    {
        pr_err("Start: Could not reply\n");
    }
    else
    {
        if (!dbus_connection_send(dbusConn, reply, &serial))
        {
            pr_err("Start: OOM in reply\n");
        }
    }

    dbus_message_unref(reply);
}

static void handle_msg_start(DBusMessage* msg)
{
    DBusError err;

    if (vnc_running)
    {
        reply_msg_start(dbus_message_new_method_return(msg), 1, vncscr->id);
        return;
    }

    dbus_error_init(&err);

    if (!dbus_message_get_args(msg, &err, DBUS_TYPE_STRING, &relay_server_ip,
        DBUS_TYPE_UINT32, &relay_port, DBUS_TYPE_INT32, &relay_id, DBUS_TYPE_INVALID))
    {
        pr_err("Start: Could not read message. %s\n", err.message);
    }
    else
    {
        startServerReply = dbus_message_new_method_return(msg);

        vnc_running = 1;
        pthread_create(&vnc_thread, NULL, vnc_loop, NULL);
    }
}

static void handle_msg_stop(DBusMessage* msg)
{
    DBusMessage* reply;
    unsigned char result = 1;
    dbus_uint32_t serial = 0;

    reply = dbus_message_new_method_return(msg);

    if (vnc_running)
    {
        vnc_running = 0;
        pthread_join(vnc_thread, NULL);
    }

    if (!dbus_message_append_args(reply, DBUS_TYPE_BOOLEAN, &result,
        DBUS_TYPE_INVALID))
    {
        pr_err("Stop: Could not reply\n");
    }
    else
    {
        if (!dbus_connection_send(dbusConn, reply, &serial))
        {
            pr_err("Stop: OOM in reply\n");
        }
    }

    dbus_message_unref(reply);
}

static int get_framebuffer_yoffset()
{
    if(ioctl(fbfd, FBIOGET_VSCREENINFO, &scrinfo) < 0) {
        pr_err("failed to get virtual screen info\n");
        return -1;
    }

    return scrinfo.yoffset;
}

#define PIXEL_FB_TO_RFB(p,r,g,b) \
    ((p >> r) & 0x1f001f) | \
    (((p >> g) & 0x1f001f) << 5) | \
    (((p >> b) & 0x1f001f) << 10)

static void update_screen(void)
{
    unsigned int *f, *c, *r;
    int x, y, y_virtual;

    /* get virtual screen info */
    y_virtual = get_framebuffer_yoffset();
    if (y_virtual < 0)
        y_virtual = 0; /* no info, have to assume front buffer */

    varblock.min_x = varblock.min_y = INT_MAX;
    varblock.max_x = varblock.max_y = -1;

    f = (unsigned int *)fbmmap;        /* -> framebuffer         */
    c = (unsigned int *)fbbuf;         /* -> compare framebuffer */
    r = (unsigned int *)vncbuf;        /* -> remote framebuffer  */

    /* jump to right virtual screen */
    f += y_virtual * scrinfo.xres_virtual / varblock.pixels_per_int;

    for (y = 0; y < (int) scrinfo.yres; y++) {
        /* Compare every 2 pixels at a time, assuming that changes are
         * likely in pairs. */
        for (x = 0; x < (int) scrinfo.xres; x += varblock.pixels_per_int) {
            unsigned int pixel = *f;

            if (pixel != *c) {
                *c = pixel; /* update compare buffer */

                /* XXX: Undo the checkered pattern to test the
                 * efficiency gain using hextile encoding. */
                if (pixel == 0x18e320e4 || pixel == 0x20e418e3)
                    pixel = 0x18e318e3; /* still needed? */

                /* update remote buffer */
                *r = PIXEL_FB_TO_RFB(pixel,
                        varblock.r_offset,
                        varblock.g_offset,
                        varblock.b_offset);

                if (x < varblock.min_x)
                    varblock.min_x = x;
                else {
                    if (x > varblock.max_x)
                        varblock.max_x = x;
                }

                if (y < varblock.min_y)
                    varblock.min_y = y;
                else if (y > varblock.max_y)
                    varblock.max_y = y;
            }

            f++, c++;
            r++;
        }
    }

    if (varblock.min_x < INT_MAX) {
        if (varblock.max_x < 0)
            varblock.max_x = varblock.min_x;

        if (varblock.max_y < 0)
            varblock.max_y = varblock.min_y;

        pr_vdebug("Changed frame: %dx%d @ (%d,%d)...\n",
          (varblock.max_x + 2) - varblock.min_x,
          (varblock.max_y + 1) - varblock.min_y,
          varblock.min_x, varblock.min_y);

        rfbMarkRectAsModified(vncscr, varblock.min_x, varblock.min_y,
          varblock.max_x + 2, varblock.max_y + 1);

        rfbProcessEvents(vncscr, 10000); /* update quickly */
    }
}

void blank_framebuffer()
{
    int i, n = scrinfo.xres * scrinfo.yres / varblock.pixels_per_int;
    for (i = 0; i < n; i++) {
        ((int *)vncbuf)[i] = 0;
        ((int *)fbbuf)[i] = 0;
    }
}

void print_usage(char **argv)
{
    pr_info("%s [-h] [-r <relay_server_ip> -p <relay_port> -i <ID>]\n"
        "-h          : print this help\n"
        "-d          : control via DBUs\n"
        "-r <relay_server_ip> : connect to <relay_server_ip> as repeater\n"
        "-p <relay_port>   : relay_server_ip relay_port\n"
        "-i <ID>     : numerical connection ID for repeater connection\n",
        APPNAME);
}

void exit_cleanup(void)
{
    pr_info("Cleaning up...\n");

    if (use_dbus)
    {
        if (vnc_running)
        {
            vnc_running = 0;
            pthread_join(vnc_thread, NULL);
        }
        dbus_connection_close(dbusConn);
        dbus_shutdown();
    }

    cleanup_fb();
    cleanup_keys();
    cleanup_touch();
}

void sigint_handler(int arg)
{
    if (old_sigint_handler)
        old_sigint_handler(arg);

    pr_err("<break> exit.\n");
    exit(-1);
}

static void* vnc_loop()
{
    rfbClientPtr client_ptr;

    pr_info("Initializing framebuffer device " FB_DEVICE "...\n");
    init_fb();

    pr_info("Initializing Framebuffer VNC server:\n");
    pr_info("    width:  %d\n", (int)scrinfo.xres);
    pr_info("    height: %d\n", (int)scrinfo.yres);
    pr_info("    bpp:    %d\n", (int)scrinfo.bits_per_pixel);
    pr_info("    relay_port:   %d\n", (int)VNC_PORT);
    init_fb_server(0, NULL);

    pr_info("Initializing input devices...\n");
    init_keys();
    init_touch();

    vncscr->id = relay_id;

    /* Implement our own event loop to detect changes in the framebuffer. */
    while (vnc_running) {

        /* If no connection is active and an ID is requested then connect to repeater
           ID = 0 ... request an ID from server
           ID > 0 ... use this ID */
        while (((!vncscr->clientHead) || (vncscr->clientHead->protocolMajorVersion == 0)) && vnc_running)
        {
            if ((!vncscr->clientHead) && (vncscr->id >= 0))
            {
                /* give repeater some time to recover from previous connection */
                sleep(1);
                vncscr->clientHead = rfbReverseConnection(vncscr, relay_server_ip, relay_port);
                pr_info("Connecting to repeater \"%s:%d\" with ID %d\n", relay_server_ip, relay_port, vncscr->id);
            }
            if (startServerReply != NULL)
            {
                /* send back success message and relay ID to DBUS caller */
                reply_msg_start(startServerReply, 1, vncscr->id);
                startServerReply = NULL;
            }
            /* Loop until getting a client. Do not sleep here to keep thread
               responsive */
            rfbProcessEvents(vncscr, 1000 * 1000);

            if ((vncscr->clientHead) && (vncscr->clientHead->protocolMajorVersion != 0))
            {
                /* raise DBUS event */
                client_new(vncscr->clientHead);
            }
        }

        /* refresh screen every 100 ms */
        rfbProcessEvents(vncscr, 100 * 1000 /* timeout in us */);

        /* all clients closed */
        if (!vncscr->clientHead) {
            blank_framebuffer(vncbuf);
        }

        /* scan screen if at least one client has requested */
        for (client_ptr = vncscr->clientHead; client_ptr; client_ptr = client_ptr->next)
        {
            if (!sraRgnEmpty(client_ptr->requestedRegion)) {
                update_screen();
                break;
            }
        }
    }

    if (vncbuf)
    {
        free(vncbuf);
    }
    rfbScreenCleanup(vncscr);
    rfbShutdownServer(vncscr, TRUE);

    return NULL;
}

int main(int argc, char **argv)
{
    int c;
    DBusMessage* msg;

    printf(APPNAME " " APPVER "\n");

    while ((c = getopt(argc, argv, "hdi:r:p:")) != -1)
    {
        switch (c)
        {
            case 'h':
                print_usage(argv);
                exit(0);
                break;
            case 'd':
                use_dbus = 1;
                break;
            case 'r':
                relay_server_ip = optarg;
                break;
            case 'p':
                relay_port = strtoul(optarg, NULL, 10);
                break;
            case 'i':
                relay_id = strtoul(optarg, NULL, 10);
                break;
            case '?':
                if (optopt == 'i')
                {
                    pr_info("Connection ID missing\n");
                    exit(1);
                }
                else if (optopt == 'r')
                {
                    pr_info("server name missing\n");
                    exit(1);
                }
                else if (optopt == 'p')
                {
                    pr_info("relay_port missing\n");
                    exit(1);
                }
                else if (isprint(optopt))
                {
                    pr_info("Unknown option -%c\n", optopt);
                }
                break;
            default:
                abort;
        }
    }

    /* Check parameters for repeater mode */
    if (relay_id > -1)
    {
        if (relay_port == 0)
        {
            pr_info("A relay_port must be given in repeater mode.\n");
            exit(1);
        }

        if (relay_server_ip == NULL)
        {
            pr_info("A repeater server must be given in repeater mode.\n");
            exit(1);
        }
    }

    if (use_dbus)
    {
        pr_info("Initializing DBus...\n");
        init_dbus();
    }

    atexit(exit_cleanup);
    old_sigint_handler = signal(SIGINT, sigint_handler);

    if (use_dbus)
    {
        while (1)
        {
            /* Can't sleep eternally here because vnc_thread must send bus
               messages and this function does not handle this...
               see http://free-desktop-dbus.2324887.n4.nabble.com/PATCH-Iteration-wakeup-td7040.htm
             */
            dbus_connection_read_write(dbusConn, 500);

            while ((msg = dbus_connection_pop_message(dbusConn)) != NULL)
            {
                pr_info("Got DBus message: %s.%s\n",
                    dbus_message_get_interface(msg),
                    dbus_message_get_member(msg));

                if (dbus_message_is_method_call(msg, DBUS_IFACE, "Start"))
                {
                    handle_msg_start(msg);
                }
                else if (dbus_message_is_method_call(msg, DBUS_IFACE, "Stop"))
                {
                    handle_msg_stop(msg);
                }
                dbus_message_unref(msg);
            }
        }
    }
    else
    {
        vnc_running = 1;
        vnc_loop();
    }

    return 0;
}
